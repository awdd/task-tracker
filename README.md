Task Tracker
========================
This is my web-based work tracker.

I'm building it here so that I can run reports I want to run and format them the way I want to. This also allows me to play more with Symfony/Twig/Bootstrap/Git.