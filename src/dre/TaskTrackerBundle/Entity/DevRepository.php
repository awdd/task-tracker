<?php
/**
 * User: Donna.Ryan
 * Date: 3/6/14
 * Project: Task Tracker
 */

namespace dre\TaskTrackerBundle\Entity;

use Doctrine\ORM\EntityRepository;

class DevRepository extends EntityRepository
{
	public function findAllOrderBy( $order, $dir )
	{
		$order = "d.".$order;
		$q = $this->createQueryBuilder( 'd' )
			->orderBy( $order, $dir )
			->getQuery();
		return $q->getResult();
	}

	public function findAllBySearchTerm( $searchterm )
	{
		$q = $this->createQueryBuilder( 'd' )
			->where('d.name LIKE :searchterm' )
			->setParameter('searchterm', '%'.$searchterm.'%')
			->getQuery();

		return $q->getResult();
	}

	public function breakoutByDev( $dataArray )
	{
		// first, get all our devs
		$devs = $this->findAllOrderBy( 'id', 'ASC' );
		$byDev  = '';

		foreach( $devs as $it=>$item )
		{
			$byDev[$it]['devid'] = $item->getId();
			$byDev[$it]['devname'] = $item->getName();
			$byDev[$it]['taskcount'] = 0;
            //print_r( $dataArray );

			foreach( $dataArray as $data )
			{
				if( $data['devid'] == $item->getId() )
				{ $byDev[$it]['taskcount']++; }
			}


		}
		return $byDev;
	}

	private function __debug( $printme )
	{
		print_r( "debug: <br />");
		print_r( "<pre>" );
		print_r( $printme );
		print_r( "</pre>" );
	}
}
