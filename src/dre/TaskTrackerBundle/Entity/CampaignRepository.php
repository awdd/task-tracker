<?php
/**
 * User: Donna.Ryan
 * Date: 3/6/14
 * Project: Task Tracker
 */

namespace dre\TaskTrackerBundle\Entity;

use Doctrine\ORM\EntityRepository;

class CampaignRepository  extends EntityRepository
{
	public function findAllOrderBy( $order, $dir, $display )
	{
		$order = "c.".$order;
		$today = date('Y-m-d 00:00:00');

		if( $display == 'all')
		{
			$q = $this->createQueryBuilder( 'c' )
				->orderBy( $order, $dir )
				->getQuery();
		}
		elseif( $display == 'active')
		{
			/*
			if( $display == 'active')           { $gtlt = '>='; }
			elseif( $display == 'inactive')     { $gtlt = '<='; }
			*/
			$q = $this->createQueryBuilder( 'c' )
				->where("c.end >= '". $today ."'" )
				->orderBy( $order, $dir )
				->getQuery();
		}
		elseif( $display == 'inactive')
		{
			$q = $this->createQueryBuilder( 'c' )
				->where("c.end < '". $today ."'" )
				->orderBy( $order, $dir )
				->getQuery();
		}
		return $q->getResult();
	}

	public function findAllBySearchTerm( $searchterm )
	{
		$q = $this->createQueryBuilder( 'c' )
			->where('c.name LIKE :searchterm OR c.added LIKE :searchterm OR c.updated LIKE :searchterm OR c.ionum LIKE :searchterm OR c.dfplineid LIKE :searchterm' )
			->setParameter('searchterm', '%'.$searchterm.'%')
			->getQuery();

		return $q->getResult();
	}

	public function findAllByDateRange( $field, $start, $end )
	{
		$q = $this->createQueryBuilder( 'c' )
			->where(':field <= :start AND :field >= :end' )
			->setParameter('field', "c.".$field)
			->setParameter('start', $start)
			->setParameter('end', $end)
			->getQuery();

		return $q->getResult();
	}

	public function listifyItems( $campaigns )
	{
		$today      = new \DateTime();
		$list       = '';

		foreach( $campaigns as $it=>$item )
		{
			$list[$it]['id']            = $item->getId();
			$list[$it]['ionum']         = $item->getIonum();
            $list[$it]['dfplineid']     = $item->getDfplineid();
			$list[$it]['jiraid']        = $item->getJiraid();
			$list[$it]['name']          = $item->getName();
			$list[$it]['basecamp']      = $item->getBasecamp();
			$list[$it]['taskcount']     = count( $item->getTask() );
			$list[$it]['sov']           = $item->getSov();

			// Date Things
			$list[$it]['start']         = $item->getStart( new \DateTime() );
			$list[$it]['end']           = $item->getEnd( new \DateTime() );
			$list[$it]['added']         = $item->getAdded( new \DateTime() );
			$list[$it]['updated']       = $item->getUpdated( new \DateTime() );

			// Manager
			$list[$it]['managerid']     = $item->getManagerid();
			$list[$it]['managername']   = $item->getManager()->getName();

			// Dev
			$list[$it]['devid']         = $item->getDevid();
			$list[$it]['devname']       = $item->getDev()->getName();

            // Coordinator
            if( $item->getCoordinatorid() != '' )
            {
                $list[$it]['coordinatorid']         = $item->getCoordinatorid();
                $list[$it]['coordinatorname']       = $item->getCoordinator()->getName();
            }
            else
            {
                $list[$it]['coordinatorid']         = '';
                $list[$it]['coordinatorname']       = '';
            }


            // Reach
			if( $item->getReachid() != '' )
			{
				$list[$it]['reachid']       = $item->getReachid();
				$list[$it]['reachname']     = $item->getReach()->getName();
			}
			else
			{
				$list[$it]['reachid']       = '';
				$list[$it]['reachname']     = '';
			}
		}

		//$this->__debug( $list );
		return $list;
	}

	public function countActive( $whom )
	{
		$today = date('Y-m-d 00:00:00');

		if( $whom == '' )
		{
			$q = $this->createQueryBuilder( 'c' )
				->select('count(c.id)')
				->where("c.end >= '". $today ."'" )
				->getQuery();
		}
		else
		{
			$q = $this->createQueryBuilder( 'c' )
				->select('count(c.id)')
				->where("c.end >= '". $today ."' AND c.devid = :devid" )
				->setParameter('devid', $whom)
				->getQuery();
		}
		return $q->getSingleScalarResult();
	}

	private function __debug( $printme )
	{
		print_r( "<pre>" );
		print_r( $printme );
		print_r( "</pre>" );
	}
}
