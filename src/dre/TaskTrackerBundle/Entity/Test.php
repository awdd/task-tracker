<?php
/**
 * User: donna.ryan
 * Date: 10/21/2014
 * Project: Task Tracker
 */

namespace dre\TaskTrackerBundle\Entity;


class Test
{
	protected $testAd;
	protected $file;
	protected $dfp;
	protected $showad;
	// database entries
	protected $id;
	protected $tempAd;

	/**
	 * @return mixed
	 */
	public function getShowad()
	{
		return $this->showad;
	}

	/**
	 * @param mixed $showad
	 */
	public function setShowad($showad)
	{
		$this->showad = $showad;
	}

	/**
	 * @return mixed
	 */
	public function getFile()
	{
		return $this->file;
	}

	/**
	 * @param mixed $file
	 */
	public function setFile($file)
	{
		$this->file = $file;
	}

	/**
	 * @return mixed
	 */
	public function getTestAd()
	{
		return $this->testAd;
	}

	/**
	 * @param mixed $testAd
	 */
	public function setTestAd($testAd)
	{
		$this->testAd = $testAd;
	}

	/**
	 * @return mixed
	 */
	public function getDfp()
	{
		return $this->dfp;
	}

	/**
	 * @param mixed $dfp
	 */
	public function setDfp($dfp)
	{
		$this->dfp = $dfp;
	}

	/**
	 * @return mixed
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param mixed $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}

	/**
	 * @return mixed
	 */
	public function getTempAd()
	{
		return $this->tempAd;
	}

	/**
	 * @param mixed $tempAd
	 */
	public function setTempAd($tempAd)
	{
		$this->tempAd = $tempAd;
	}


}
