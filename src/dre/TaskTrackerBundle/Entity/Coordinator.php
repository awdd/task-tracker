<?php
namespace dre\TaskTrackerBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;    // remove this if I don't use any relationships

class Coordinator
{
	protected $id;
	protected $name;
	protected $added;
	protected $updated;
	protected $campaign;

	public function __construct()
	{
		$this->campaign = new ArrayCollection();
	}

	/**
	 * @param datetime $added
	 */
	public function setAdded($added)        { $this->added = $added; }

	/**
	 * @return datetime
	 */
	public function getAdded()              { return $this->added; }

	/**
	 * @param integer $id
	 */
	public function setId($id)              { $this->id = $id; }

	/**
	 * @return integer
	 */
	public function getId()                 { return $this->id; }

	/**
	 * @param string $name
	 */
	public function setName($name)          { $this->name = $name; }

	/**
	 * @return string
	 */
	public function getName()               { return $this->name; }

	/**
	 * @param datetime $updated
	 */
	public function setUpdated($updated)    { $this->updated = $updated; }

	/**
	 * @return datetime
	 */
	public function getUpdated()            { return $this->updated; }

	/**
	 * @param \Doctrine\Common\Collections\ArrayCollection $campaign
	 */
	public function setCampaign($campaign)
	{
		$this->campaign = $campaign;
	}

	/**
	 * @return \Doctrine\Common\Collections\ArrayCollection
	 */
	public function getCampaign()
	{
		return $this->campaign;
	}


}
