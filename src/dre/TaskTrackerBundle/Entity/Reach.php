<?php
/**
 * User: donna.ryan
 * Date: 1/6/14
 * Time: 11:59 AM
 */

namespace dre\TaskTrackerBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;

class Reach
{
	protected $id;
	protected $name;
	protected $added;
	protected $updated;
    protected $campaign;

	public function __construct()
	{
        $this->campaign = new ArrayCollection();
	}

	/* Getters */
	public function getId()         { return $this->id; }
	public function getName()       { return $this->name; }
	public function getAdded()      { return $this->added; }
	public function getUpdated()    { return $this->updated; }

	/* Setters */
	public function setId($v)       { $this->id = $v;       return $this; }
	public function setName($v)     { $this->name = $v;     return $this; }
	public function setAdded($v)    { $this->added = $v;    return $this; }
	public function setUpdated($v)  { $this->updated = $v;  return $this; }

    /**
     * @param \Doctrine\Common\Collections\ArrayCollection $task
     */
    public function setCampaign($campaign)
    {
        $this->campaign = $campaign;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getCampaign()
    {
        return $this->$campaign;
    }


}
