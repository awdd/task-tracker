<?php
/**
 * User: donna.ryan
 * Date: 1/6/14
 * Time: 5:14 PM
 */

namespace dre\TaskTrackerBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;


class Campaign
{
	protected $id;
	protected $name;
	protected $ionum;
	protected $start;
	protected $end;
	protected $added;
	protected $updated;
	protected $task;
	protected $managerid;
	protected $manager;
    protected $devid;
    protected $dev;
    protected $coordinatorid;
    protected $dfplineid;
	protected $jiraid;
    protected $coordinator;
	protected $basecamp;
	protected $reachid;
	protected $reach;
	protected $sov;

	function __construct()
	{
		$this->task             = new ArrayCollection();
		$this->manager          = new ArrayCollection();
        $this->dev              = new ArrayCollection();
		$this->reach            = new ArrayCollection();
        $this->coordinator      = new ArrayCollection();
	}

	/* Getters */
	public function getId()				{ return $this->id; }
	public function getName()			{ return $this->name; }
	public function getIonum()          { return $this->ionum; }
	public function getStart()          { return $this->start; }
	public function getEnd()            { return $this->end; }
	public function getAdded()			{ return $this->added; }
	public function getUpdated()		{ return $this->updated; }
	public function getManagerid()      { return $this->managerid; }
    public function getDevid()          { return $this->devid; }
	public function getBasecamp()       { return $this->basecamp; }
	public function getReachid()        { return $this->reachid; }
	public function getSov()            { return $this->sov; }
    public function getCoordinatorid()  { return $this->coordinatorid; }
    public function getDfplineid()      { return $this->dfplineid; }
	public function getJiraid()         { return $this->jiraid; }

	/* Setters */
	public function setId( $v )			        { $this->id = $v;		            return $this; }
	public function setName( $v )		        { $this->name = $v;		            return $this; }
	public function setIonum( $v )              { $this->ionum = $v;                return $this; }
	public function setStart( $v )              { $this->start = $v;                return $this; }
	public function setEnd( $v )                { $this->end = $v;                  return $this; }
	public function setAdded( $v )		        { $this->added = $v;	            return $this; }
	public function setUpdated( $v )	        { $this->updated = $v;	            return $this; }
	public function setManagerid( $managerid )  { $this->managerid = $managerid;    return $this; }
    public function setDevid( $devid )          { $this->devid = $devid;            return $this; }
	public function setBasecamp( $basecamp )    { $this->basecamp = $basecamp;      return $this; }
	public function setReachid( $reachid )      { $this->reachid = $reachid;        return $this; }
	public function setSov( $sov )              { $this->sov = $sov;                return $this; }
    public function setCoordinatorid( $coordinatorid )  { $this->coordinatorid = $coordinatorid;    return $this; }
    public function setDfplineid( $dfplineid )  { $this->dfplineid = $dfplineid;    return $this; }
	public function setJiraid( $jiraid )        { $this->jiraid = $jiraid;          return $this; }

	/**
	 * @param \Doctrine\Common\Collections\ArrayCollection $reach
	 */
	public function setReach($reach)
	{
		$this->reach = $reach;
	}

	/**
	 * @return \Doctrine\Common\Collections\ArrayCollection
	 */
	public function getReach()
	{
		return $this->reach;
	}

    /**
     * @param \Doctrine\Common\Collections\ArrayCollection $dev
     */
    public function setDev($dev)
    {
        $this->dev = $dev;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getDev()
    {
        return $this->dev;
    }

	/**
	 * @param \Doctrine\Common\Collections\ArrayCollection $task
	 */
	public function setTask($task)
	{
		$this->task = $task;
	}

	/**
	 * @return \Doctrine\Common\Collections\ArrayCollection
	 */
	public function getTask()
	{
		return $this->task;
	}

	public function getManager()
	{
		return $this->manager;
	}

	public function setManager( \dre\TaskTrackerBundle\Entity\Manager $manager )
	{
		$this->manager = $manager;
		return $this;
	}

    /**
     * @return ArrayCollection
     */
    public function getCoordinator()
    {
        return $this->coordinator;
    }

    /**
     * @param ArrayCollection $coordinator
     */
    public function setCoordinator($coordinator)
    {
        $this->coordinator = $coordinator;
        return $this;
    }

}
