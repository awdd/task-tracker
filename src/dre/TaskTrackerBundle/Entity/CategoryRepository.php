<?php
/**
 * User: donna.ryan
 * Date: 2/20/14
 * Project: Task Tracker
 */

namespace dre\TaskTrackerBundle\Entity;

use Doctrine\ORM\EntityRepository;

class CategoryRepository extends EntityRepository
{
	public function findAllOrderBy( $order, $dir )
	{
		$order = "c.".$order;
		$q = $this->createQueryBuilder( 'c' )
			->orderBy( $order, $dir )
			->getQuery();
		return $q->getResult();
	}

	public function findAllBySearchTerm( $searchterm )
	{
		$q = $this->createQueryBuilder( 'c' )
			->where('c.name LIKE :searchterm OR c.added LIKE :searchterm OR c.updated LIKE :searchterm')
			->setParameter('searchterm', '%'.$searchterm.'%')
			->getQuery();

		return $q->getResult();
	}

	public function breakoutByCategory( $dataArray )
	{
		// pull all the categories
		$categories = $this->findAllOrderBy('id', 'ASC');
		$byCat = '';

		foreach( $categories as $it=>$item )
		{
			$byCat[$it]['catid'] = $item->getId();
			$byCat[$it]['catname'] = $item->getName();
			$byCat[$it]['taskcount'] = 0;
			foreach( $dataArray as $data )
			{
				if( $data['categoryid'] == $item->getId() )
				{ $byCat[$it]['taskcount']++; }
			}
		}

		return $byCat;
	}
}
