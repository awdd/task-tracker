<?php
namespace dre\TaskTrackerBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;    // remove this if I don't use any relationships

class Dev
{
	protected $id;
	protected $name;
	protected $basecamp;
	protected $added;
	protected $updated;
    protected $task;
	protected $campaign;

	public function __construct()
	{
        $this->task = new ArrayCollection();
		$this->campaign = new ArrayCollection();
	}

    /**
     * @param \Doctrine\Common\Collections\ArrayCollection $task
     */
    public function setTask($task)
    {
        $this->task = $task;
    }/**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getTask()
    {
        return $this->task;
    }

	/**
	 * @param datetime $added
	 */
	public function setAdded($added)        { $this->added = $added; }

	/**
	 * @return datetime
	 */
	public function getAdded()              { return $this->added; }

	/**
	 * @param integer $id
	 */
	public function setId($id)              { $this->id = $id; }

	/**
	 * @return integer
	 */
	public function getId()                 { return $this->id; }

	/**
	 * @param string $name
	 */
	public function setName($name)          { $this->name = $name; }

	/**
	 * @return string
	 */
	public function getName()               { return $this->name; }

	/**
	 * @param string $tip
	 */
	public function setBasecamp($basecamp)        { $this->basecamp = $basecamp; }

	/**
	 * @return string
	 */
	public function getBasecamp()              { return $this->basecamp; }

	/**
	 * @param datetime $updated
	 */
	public function setUpdated($updated)    { $this->updated = $updated; }

	/**
	 * @return datetime
	 */
	public function getUpdated()            { return $this->updated; }

	/**
	 * @return ArrayCollection
	 */
	public function getCampaign() { return $this->campaign; }

	/**
	 * @param ArrayCollection $campaign
	 */
	public function setCampaign($campaign)  { $this->campaign = $campaign; }


}
