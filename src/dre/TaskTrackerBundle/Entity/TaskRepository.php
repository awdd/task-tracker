<?php
/**
 * User: donna.ryan
 * Date: 2/20/14
 * Project: Task Tracker
 */

namespace dre\TaskTrackerBundle\Entity;

use Doctrine\ORM\EntityRepository;

class TaskRepository extends EntityRepository
{
	public function findAllOrderBy( $order, $dir )
	{
		$order = "t.".$order;
		$q = $this->createQueryBuilder( 't' )
			->orderBy( $order, $dir )
			->getQuery();
		return $q->getResult();
	}

	public function findAllBySearchTerm( $searchterm )
	{
		$q = $this->createQueryBuilder( 't' )
			->where('t.name LIKE :searchterm OR t.due LIKE :searchterm OR t.completed LIKE :searchterm OR t.runstart LIKE :searchterm OR t.runend LIKE :searchterm or t.assets LIKE :searchterm' )
			->setParameter('searchterm', '%'.$searchterm.'%')
			->getQuery();

		return $q->getResult();
	}

	public function findIncomplete( $order, $dir )
	{
		$order = "t.".$order;
		$q = $this->createQueryBuilder( 't' )
			->where('t.completed IS NULL')
			->orderBy( $order, $dir )
			->getQuery();

		return $this->listifyTasks( $q->getResult() );
	}

	public function findIncompleteForDateRange( $order, $dir, $start, $end )
	{
		$order = "t.".$order;
		$q = $this->createQueryBuilder( 't' )
			->where('t.completed IS NULL AND t.due >= :start AND t.due <= :end')
			->orderBy( $order, $dir )
			->setParameter('start', $start)
			->setParameter('end', $end)
			->getQuery();

		return $this->listifyTasks( $q->getResult() );
	}

	public function findIncompleteOverdue( $order, $dir, $today )
	{
		$order = "t.".$order;
		$q = $this->createQueryBuilder( 't' )
			->where('t.completed IS NULL AND t.due < :today')
			->orderBy( $order, $dir )
			->setParameter('today', $today)
			->getQuery();

		return $this->listifyTasks( $q->getResult() );
	}
	public function findComplete( $order, $dir )
	{
		$order = "t.".$order;
		$q = $this->createQueryBuilder( 't' )
			->where('t.completed IS NOT NULL')
			->orderBy( $order, $dir )
			->getQuery();

		return $this->listifyTasks( $q->getResult() );
	}

	public function listifyTasks( $tasks )
	{
		$today          = new \DateTime();
		$today->setTime( 0, 0, 0 ); // sets the time to match the 0's that will come from the database
		$tomrw       = new \DateTime();
		$tomrw->setTime( 0, 0, 0 ); // sets the time to match the 0's that will come from the database
		$tomrw->setDate( $tomrw->format('Y'), $tomrw->format('m'), ( $tomrw->format('d')+1 ) );

		$tasklist       = '';

		foreach( $tasks as $it=>$task )
		{
			// Tasky bits
			$tasklist[$it]['id']                = $task->getId();
			$tasklist[$it]['name']              = $task->getName();
			$tasklist[$it]['basecamp']          = $task->getBasecamp();
			// Dates
			$tasklist[$it]['due']               = $task->getDue( new \DateTime() );
			$tasklist[$it]['completed']         = $task->getCompleted( new \DateTime() );
			$tasklist[$it]['runstart']          = $task->getRunstart( new \DateTime() );
			$tasklist[$it]['runend']            = $task->getRunend( new \DateTime() );
			$tasklist[$it]['added']             = $task->getAdded( new \DateTime() );
			$tasklist[$it]['assets']            = $task->getAssets( new \DateTime() );
			// Campaign
			$tasklist[$it]['campaignid']        = $task->getCampaignid();
			$tasklist[$it]['campaignname']      = $task->getCampaign()->getName();
			$tasklist[$it]['campaignionum']     = $task->getCampaign()->getIonum();
			$tasklist[$it]['campaignreachname'] = $task->getCampaign()->getReach()->getName();
			// Category
			$tasklist[$it]['categoryid']        = $task->getCategoryid();
			$tasklist[$it]['categoryname']      = $task->getCategory()->getName();
			$tasklist[$it]['categorytip']       = $task->getCategory()->getTip();
			// Dev
			$tasklist[$it]['devid']             = $task->getDevid();
			$tasklist[$it]['devname']           = $task->getDev()->getName();
			// Manager
			$tasklist[$it]['managerid']        = $task->getManagerid();
			$tasklist[$it]['managername']      = $task->getManager()->getName();
			// Due-ness
			$tasklist[$it]['tomorrow']      = $tomrw;
			if( $tasklist[$it]['completed'] != "")          { $tasklist[$it]['dueness'] = 'muted'; }
			else
			{
				if( $tasklist[$it]['due'] == $tomrw )       { $tasklist[$it]['dueness'] = 'list-upcoming'; }
				elseif( $tasklist[$it]['due'] == $today)    { $tasklist[$it]['dueness'] = 'list-current'; }
				elseif( $tasklist[$it]['due'] < $today )    { $tasklist[$it]['dueness'] = 'list-overdue'; }
				else                                        { $tasklist[$it]['dueness'] = ''; }
			}
		}
		return $tasklist;
	}

	public function getTaskYears()
	{
		$q = $this->createQueryBuilder( 't' )
			->select('t.due')
			->groupBy('t.due')
			->getQuery();
		$taskObj = $q->getScalarResult();

		foreach( $taskObj as $it=>$task )
		{
			$tempdates = split( '-', $task['due'] );
			$dates[]['year'] = $tempdates[0];
		}
		for( $i = 0; $i < count( $dates ); $i++ )
		{
			if( $i == 0 )
			{
				$years[]['year'] = $dates[$i]['year'];
				$preventry = $dates[$i]['year'];
			}
			else
			{
				if( $dates[$i]['year'] != $preventry )
				{
					$years[]['year'] = $dates[$i]['year'];
					$preventry = $dates[$i]['year'];
				}
			}

		}
		return $years;
	}

	public function getTasksCompletedInMonth( $year, $mon )
	{
		$monthStart = $year . '-' . $mon . '-01 00:00:00';
		$monthEnd = date('Y-m-d 00:00:00', gmmktime(0,0,0, ($mon+1), 0, $year ));

		$q = $this->createQueryBuilder( 't' )
			->where( 't.completed >= :start AND t.completed <= :end' )
			->orderBy( 't.due' )
			->setParameter( 'start', $monthStart )
			->setParameter( 'end', $monthEnd )
			->getQuery();

		return $this->listifyTasks( $q->getResult() );
	}

	public function breakoutByTask( $dataArray )
	{
		$numbers['total'] = count( $dataArray );
		$numbers['dates']['sdad'] = 0;
		$numbers['dates']['sdaa'] = 0;
		$numbers['dates']['sdcm'] = 0;
		$numbers['dates']['rest'] = 0;

		foreach( $dataArray as $data )
		{
			// same day added, due & completed
			// same day assets & completed, added != due
			// everything else
			if( ($data['due'] == $data['added']) && ($data['due'] == $data['completed']) )
			{
				$numbers['dates']['sdad']++;
			}
			elseif( ($data['completed'] == $data['assets']) )
			{
				$numbers['dates']['sdaa']++;
			}
			elseif( ($data['completed'] == $data['due']) )
			{
				$numbers['dates']['sdcm']++;
			}
			else { $numbers['dates']['rest']++; }
		}
		// add the same days together
		$numbers['dates']['sdt'] = $numbers['dates']['sdad'] + $numbers['dates']['sdaa'] + $numbers['dates']['sdcm'];
		//$numbers['dates']['rest'] = $numbers['total'] - $numbers['dates']['sdt'];
		return $numbers;
	}

	public function countActive( $whom )
	{
		if( $whom == '' )
		{
			$q = $this->createQueryBuilder( 't' )
				->select('count(t.id)')
				->where("t.completed IS NULL" )
				->getQuery();
		}
		else
		{
			$q = $this->createQueryBuilder( 't' )
				->select('count(t.id)')
				->where("t.completed IS NULL AND t.devid = :devid" )
				->setParameter('devid', $whom)
				->getQuery();
		}
		return $q->getSingleScalarResult();
	}
}
