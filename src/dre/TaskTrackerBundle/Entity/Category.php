<?php
namespace dre\TaskTrackerBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;    // remove this if I don't use any relationships

class Category
{
	protected $id;
	protected $name;
	protected $added;
	protected $updated;
	protected $tip;
	protected $time;
    protected $task;

	public function __construct()
	{
        $this->task = new ArrayCollection();
	}

    /**
     * @param \Doctrine\Common\Collections\ArrayCollection $task
     */
    public function setTask($task)
    {
        $this->task = $task;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getTask()
    {
        return $this->task;
    }

	/**
	 * @param mixed $time
	 */
	public function setTime($time)
	{
		$this->time = $time;
	}

	/**
	 * @return mixed
	 */
	public function getTime()
	{
		return $this->time;
	}

	/**
	 * @param datetime $added
	 */
	public function setAdded($added)        { $this->added = $added; }

	/**
	 * @return datetime
	 */
	public function getAdded()              { return $this->added; }

	/**
	 * @param integer $id
	 */
	public function setId($id)              { $this->id = $id; }

	/**
	 * @return integer
	 */
	public function getId()                 { return $this->id; }

	/**
	 * @param string $name
	 */
	public function setName($name)          { $this->name = $name; }

	/**
	 * @return string
	 */
	public function getName()               { return $this->name; }

	/**
	 * @param string $tip
	 */
	public function setTip($tip)            { $this->tip = $tip; }

	/**
	 * @return string
	 */
	public function getTip()                { return $this->tip; }

	/**
	 * @param datetime $updated
	 */
	public function setUpdated($updated)    { $this->updated = $updated; }

	/**
	 * @return datetime
	 */
	public function getUpdated()            { return $this->updated; }


}
