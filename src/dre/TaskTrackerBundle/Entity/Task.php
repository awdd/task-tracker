<?php
/**
 * User: donna.ryan
 * Date: 1/9/14
 * Project: Task Tracker
 */

namespace dre\TaskTrackerBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use dre\TaskTrackerBundle\dreTaskTrackerBundle;

class Task
{
	protected $id;
	protected $name;
	protected $due;
	protected $added;
	protected $assets;
	protected $completed;
	protected $updated;
	protected $runstart;
	protected $runend;
	protected $basecamp;
	protected $campaignid;
	protected $campaign;
    protected $managerid;
    protected $manager;
    protected $devid;
    protected $dev;
	protected $categoryid;
	protected $category;
    protected $reachid;
    protected $reach;
	protected $dueupdates;

	function __construct()
	{
		$this->campaign = new ArrayCollection();
        $this->manager = new ArrayCollection();
        $this->dev = new ArrayCollection();
		$this->category = new ArrayCollection();
        $this->reach = new ArrayCollection();
	}

	/**
	 * @param mixed $dueupdates
	 */
	public function setDueupdates($dueupdates)
	{
		$this->dueupdates = $dueupdates;
	}

	/**
	 * @return mixed
	 */
	public function getDueupdates()
	{
		return $this->dueupdates;
	}



    /**
     * @param \Doctrine\Common\Collections\ArrayCollection $reach
     */
    public function setReach($reach)
    {
        $this->reach = $reach;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getReach()
    {
        return $this->reach;
    }

    /**
     * @param mixed $reachid
     */
    public function setReachid($reachid)
    {
        $this->reachid = $reachid;
    }

    /**
     * @return mixed
     */
    public function getReachid()
    {
        return $this->reachid;
    }

	/**
	 * @param \Doctrine\Common\Collections\ArrayCollection $category
	 */
	public function setCategory($category)
	{
		$this->category = $category;
	}

	/**
	 * @return \Doctrine\Common\Collections\ArrayCollection
	 */
	public function getCategory()
	{
		return $this->category;
	}

	/**
	 * @param mixed $categoryid
	 */
	public function setCategoryid($categoryid)
	{
		$this->categoryid = $categoryid;
	}

	/**
	 * @return mixed
	 */
	public function getCategoryid()
	{
		return $this->categoryid;
	}

    /**
     * @param \Doctrine\Common\Collections\ArrayCollection $dev
     */
    public function setDev($dev)
    {
        $this->dev = $dev;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getDev()
    {
        return $this->dev;
    }

    /**
     * @param mixed $devid
     */
    public function setDevid($devid)
    {
        $this->devid = $devid;
    }

    /**
     * @return mixed
     */
    public function getDevid()
    {
        return $this->devid;
    }

    /**
     * @param \Doctrine\Common\Collections\ArrayCollection $manager
     */
    public function setManager($manager)
    {
        $this->manager = $manager;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getManager()
    {
        return $this->manager;
    }

    /**
     * @param mixed $managerid
     */
    public function setManagerid($managerid)
    {
        $this->managerid = $managerid;
    }

    /**
     * @return mixed
     */
    public function getManagerid()
    {
        return $this->managerid;
    }



	/**
	 * @param mixed $added
	 */
	public function setAdded($added)
	{
		$this->added = $added;
	}

	/**
	 * @return mixed
	 */
	public function getAdded()
	{
		return $this->added;
	}

	/**
	 * @param mixed $assets
	 */
	public function setAssets($assets)
	{
		$this->assets = $assets;
	}

	/**
	 * @return mixed
	 */
	public function getAssets()
	{
		return $this->assets;
	}

	/**
	 * @param mixed $basecamp
	 */
	public function setBasecamp($basecamp)
	{
		$this->basecamp = $basecamp;
	}

	/**
	 * @return mixed
	 */
	public function getBasecamp()
	{
		return $this->basecamp;
	}

	/**
	 * @param mixed $completed
	 */
	public function setCompleted($completed)
	{
		$this->completed = $completed;
	}

	/**
	 * @return mixed
	 */
	public function getCompleted()
	{
		return $this->completed;
	}

	/**
	 * @param mixed $due
	 */
	public function setDue($due)
	{
		$this->due = $due;
	}

	/**
	 * @return mixed
	 */
	public function getDue()
	{
		return $this->due;
	}

	/**
	 * @param mixed $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}

	/**
	 * @return mixed
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param mixed $name
	 */
	public function setName($name)
	{
		$this->name = $name;
	}

	/**
	 * @return mixed
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @param mixed $run_end
	 */
	public function setRunend($run_end)
	{
		$this->runend = $run_end;
	}

	/**
	 * @return mixed
	 */
	public function getRunend()
	{
		return $this->runend;
	}

	/**
	 * @param mixed $run_start
	 */
	public function setRunstart($run_start)
	{
		$this->runstart = $run_start;
	}

	/**
	 * @return mixed
	 */
	public function getRunstart()
	{
		return $this->runstart;
	}

	/**
	 * @param mixed $updated
	 */
	public function setUpdated()
	{
		//$this->updated = $updated;
		// set it by default so we don't have to!
		$this->updated = new \DateTime("now");
	}

	/**
	 * @return mixed
	 */
	public function getUpdated()
	{
		return $this->updated;
	}

	/**
	 * @param \Doctrine\Common\Collections\ArrayCollection $campaign
	 */
	public function setCampaign( \dre\TaskTrackerBundle\Entity\Campaign $campaign)
	{
		$this->campaign = $campaign;
	}

	/**
	 * @return \Doctrine\Common\Collections\ArrayCollection
	 */
	public function getCampaign()
	{
		return $this->campaign;
	}

	/**
	 * @param mixed $campaignid
	 */
	public function setCampaignid($campaignid)
	{
		$this->campaignid = $campaignid;
	}

	/**
	 * @return mixed
	 */
	public function getCampaignid()
	{
		return $this->campaignid;
	}





}
