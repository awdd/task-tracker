<?php
/**
 * User: Donna.Ryan
 * Date: 3/6/14
 * Project: Task Tracker
 */

namespace dre\TaskTrackerBundle\Entity;

use Doctrine\ORM\EntityRepository;

class ManagerRepository extends EntityRepository
{
	public function findAllOrderBy( $order, $dir )
	{
		$order = "m.".$order;
		$q = $this->createQueryBuilder( 'm' )
			->orderBy( $order, $dir )
			->getQuery();
		return $q->getResult();
	}

	public function findAllBySearchTerm( $searchterm )
	{
		$q = $this->createQueryBuilder( 'm' )
			->where('m.name LIKE :searchterm' )
			->setParameter('searchterm', '%'.$searchterm.'%')
			->getQuery();

		return $q->getResult();
	}

}
