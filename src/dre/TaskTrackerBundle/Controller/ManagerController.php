<?php

namespace dre\TaskTrackerBundle\Controller;

use dre\TaskTrackerBundle\Form\Type\ManagerType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use dre\TaskTrackerBundle\Entity\Manager;

class ManagerController extends Controller
{
	var $managerRepo = "dreTaskTrackerBundle:Manager";

	public function indexAction()
	{
		$managers = $this->getDoctrine()->getRepository($this->managerRepo)->findAll();
		return $this->render('dreTaskTrackerBundle:Manager:list.html.twig', array('managers' => $managers));
	}

	public function addAction( Request $request )
	{
		$manager = new Manager();

		$form = $this->CreateForm( new ManagerType(), $manager );

		if( $request->isMethod('POST') )
		{   // we possibly have a new entry to add!
			$form->bind( $request );

			if( $form->isValid() )
			{   // save the new task dev
				$timestamp = new \DateTime();
				$manager->setAdded( $timestamp );
				$manager->setUpdated( $timestamp );

				$em = $this->getDoctrine()->getManager();
				$em->persist( $manager );
				$em->flush();

				$this->get('session')->getFlashBag()->add('notice', 'Your manager has been added.');
				return $this->redirect( $this->generateUrl('dre_task_tracker_manager') );
			}
			else
			{   // there was an error somewhere
				$formError = $form->getErrorsAsString();
				$notice = 'Your manager has not been added. Please try again. ' . $formError;
				$this->get('session')->getFlashBag()->add('notice', $notice );
				return $this->render('dreTaskTrackerBundle:Manager:add.html.twig', array(
					'form' => $form->createView()
				));
			}
		}
		else
		{   // we have no post values, so show the form to add a new dev
			return $this->render('dreTaskTrackerBundle:Manager:add.html.twig', array(
				'form' => $form->createView()
			));
		}
	}

	public function editAction( $id )
	{   // this is the view for editing
		$manager = $this->getDoctrine()->getRepository( $this->managerRepo )->find( $id );
		$form = $this->createForm( new ManagerType(), $manager );

		return $this->render('dreTaskTrackerBundle:Manager:edit.html.twig', array(
			'form' => $form->createView(),
			'id' => $id,
			'manager' => $manager,
		));
	}

	public function updateAction( Request $request )
	{   // this is the actual updating portion of edit
		$update = new Manager();
		$form = $this->createForm( new ManagerType(), $update );

		if( $request->isMethod('POST') )
		{
			$form->bind( $request );
			// get the original dev info so we can see what's been changed
			$manager = $this->getDoctrine()->getRepository( $this->managerRepo )->find( $update->getId() );

			if( $form->isValid() )
			{
				if( $update->getName() != $manager->getName() )
				{
					$manager->setName( $update->getName() );
					$manager->setUpdated( new \DateTime() );
				}
				$em = $this->getDoctrine()->getManager();
				$em->flush();

				$notice = "Your manager changes have been saved.";
				$this->get('session')->getFlashBag()->add('notice', $notice );
				return $this->redirect( $this->generateUrl('dre_task_tracker_manager') );
			}
			else
			{
				$formError = $form->getErrorsAsString();
				$notice = 'Your manager has not been updated. Please try again. ' . $formError;
				$this->get('session')->getFlashBag()->add('notice', $notice );
				return $this->redirect( $this->generateUrl('dre_task_tracker_manager_edit', array(
					'id' => $update->getId()
				)) );
			}
		}
		else
		{
			return $this->redirect( $this->generateUrl('dre_task_tracker_manager_edit', array(
				'id' => $update->getId()
			)) );
		}
	}

	public function deleteAction( $id )
	{   // @TODO: When I have relationships, set this to not delete if there is other items associated with this manager
		$manager = $this->getDoctrine()->getRepository( $this->managerRepo )->findOneById( $id );

		$em = $this->getDoctrine()->getManager();
		$em->remove( $manager );
		$em->flush();

		$notice = 'Manager '. $manager->getName() . ' has been deleted.';
		$this->get('session')->getFlashBag()->add( 'notice', $notice );
		return $this->redirect( $this->generateUrl('dre_task_tracker_manager') );
	}
}
