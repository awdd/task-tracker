<?php
/**
 * User: donna.ryan
 * Date: 10/21/2014
 * Project: Task Tracker
 */

namespace dre\TaskTrackerBundle\Controller;

use dre\TaskTrackerBundle\Form\Type\TakeoverCheckType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use dre\TaskTrackerBundle\Entity\Test;
use dre\TaskTrackerBundle\Form\Type\TestAdType;
use dre\TaskTrackerBundle\Form\Type\TestAdCompareType;


class TestController extends Controller
{
	var $mainRepo = "dreTaskTrackerBundle:Test";

	public function viewAdAction(Request $request)
	{
		$testAd = new Test();

		$form = $this->createForm(new TestAdType(), $testAd);

		if ($request->isMethod("POST"))
		{
			$form->bind($request);
			if ($form->isValid())
			{
				// let's get ad-bits & info
				$ad['size'] = $this->pullAdSize( $testAd->getTestAd() );
				$ad['src'] = $this->pullAdSrc( $testAd->getTestAd() );

				// now let's do some clean up on the tags
				$cleanedAd = $this->removeDfpMacros( $testAd->getTestAd() );

				return $this->render($this->mainRepo . ":ad.html.twig", array(
					'form'   => $form->createView(),
					'showad' => $testAd->getShowad(),
					'adview' => $cleanedAd,
					'adinfo' => $ad
				));

			}
			else
			{
				$notice = "There is an error with the tag or nothing to display. ";
				$this->get('session')->getFlashBag()->add('notice', $notice);
				return $this->redirect($this->generateUrl('dre_task_tracker_test_ad'));
			}
		}
		else
		{
			$showadinfo = 0;
			return $this->render($this->mainRepo . ":ad.html.twig", array(
				'form'   => $form->createView(),
				'showad' => $showadinfo,
				'adview' => '',
				'adinfo' => ''
			));
		}
	}

	public function compareAdAction(Request $request)
	{
		$adview = new Test();

		$form = $this->createForm(new TestAdCompareType(), $adview);

		if ($request->isMethod("POST"))
		{
			$form->bind($request);
			if ($form->isValid())
			{
				// set up most of our info breakouts
				$dfpArray = $adview->getDfp();
				$fileArray = $adview->getFile();

				// perform comparisons on the two tags
				// first test - do the two match?
				$notice = 'These tags match';
				$dfptest = '';
				$filetest = '';
				if ($fileArray != $dfpArray)
				{
					$notice = 'These tags DO NOT match';
					$dfpTest = str_split($dfpArray);
					$fileTest = str_split($fileArray);
					for ($i = 0; $i < strlen($dfpArray); $i++)
					{
						if ($dfpTest[$i] != $fileTest[$i])
						{
							$dfptest[] = "Character #" . $i . " : " . $dfpTest[$i];
							$filetest[] = "Character #" . $i . " : " . $fileTest[$i];
						}
					}
				}
				$this->get('session')->getFlashBag()->add('notice', $notice);
				return $this->render($this->mainRepo . ":ad-compare.html.twig", array(
					'form'     => $form->createView(),
					'adview'   => $adview,
					'filetest' => $filetest,
					'dfptest'  => $dfptest
				));
			}
			else
			{
				$notice = "There is an error with the tag or nothing to display. ";
				$this->get('session')->getFlashBag()->add('notice', $notice);
				return $this->redirect($this->generateUrl('dre_task_tracker_test_ad'));
			}
		}
		else
		{
			return $this->render($this->mainRepo . ":ad-compare.html.twig", array(
				'form'   => $form->createView(),
				'adview' => ''
			));
		}
	}

	function compareIncAction()
	{
		return $this->render($this->mainRepo . ":ad-compare-inc.html.twig" );
	}

	private function pullAdSize( $adtag )
	{
		if( preg_match( "/sz=([0-9]{1,4}x[0-9]{1,4})/", $adtag, $adbits ) != FALSE )
		{
			return $adbits[1];
		}
	}

	private function pullAdSrc( $adtag )
	{
		$src = FALSE;
		if( preg_match("/href=\"http:\/\/?([^;]+)/i", $adtag, $href) != FALSE )
		{
			$src['href'] = $href[1];
		}
		elseif( preg_match("/src=\"\/\/?([^;]+)/i", $adtag, $jsrc) != FALSE )
		{
			$splited = split( "\"", $jsrc[1] );
			$src['javascript'] = $splited[0];
		}

		if( preg_match_all("/src=\"http:\/\/?([^;]+)/i", $adtag, $srcs) != FALSE )
		{
			if( isset($srcs[1][1]) )
			{
				$src['image'] = $srcs[1][0];
				$src['image'] = $srcs[1][1];
			}
			else
			{
				$src['javascript'] = $srcs[1][0];
			}
		}

		return $src;
	}

	private function removeDfpMacros( $adtag )
	{
		$dfpMacros = array(
			"/%%CLICK_URL_ESC%%/",
			"/\[INSERT AD SERVER CLICK REDIRECT HERE\]/",
			"/\[INSERT TIME STAMP \/ RND NUMBER HERE\]/"
		);
		return preg_replace($dfpMacros, '', $adtag);
	}

	public function takeoverChecklistAction()
	{
		$form = $this->createForm(new TakeoverCheckType());

		return $this->render($this->mainRepo . ":checklist.html.twig", array(
			'form'   => $form->createView(),
		));
	}
	/* NOTES ----------------------------------------------------------------------
		RegEx testing:
			ad size: sz=([0-9]{2,4}x[0-9]{2,4})  - finds 300x250
			domain: ([^:]*:\/\/)?([^\/]*\.)*([^\/\.]+)\.([^\/]*) 1) <a href=http:// 2) ad 3) doubleclick 4)net
			preg_match("/http:\/\/?([^;]+)/", $testAd->getTestAd(), $href);
	}

	<A HREF="http://ad.doubleclick.net/jump/N5778.126567.SHEKNOWS/B8297605.112623395;sz=160x600;ord=[timestamp]?">
	<IMG SRC="http://ad.doubleclick.net/ad/N5778.126567.SHEKNOWS/B8297605.112623395;sz=160x600;ord=[timestamp]?" BORDER=0 WIDTH=160 HEIGHT=600 ALT="Advertisement"></A>

	*/
}
