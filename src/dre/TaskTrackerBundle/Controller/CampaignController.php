<?php
/**
 * User: donna.ryan
 * Date: 1/6/14
 * Time: 4:53 PM
 */

namespace dre\TaskTrackerBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use dre\TaskTrackerBundle\Entity\Campaign;
use dre\TaskTrackerBundle\Form\Type\CampaignType;


class CampaignController extends Controller
{
	var $mainRepo = "dreTaskTrackerBundle:Campaign";

	public function indexAction($sort, $display, Request $request)
	{
		if( !$request->query->get('dir') )	{ $dir = "DESC"; }
		else								{ $dir = $request->query->get('dir'); }

		$campaigns = $this->getDoctrine()->getRepository($this->mainRepo)->findAllOrderBy($sort, $dir, $display);
		$campaignlist = $this->getDoctrine()->getRepository( $this->mainRepo )->listifyItems( $campaigns );
		return $this->render('dreTaskTrackerBundle:Campaign:list.html.twig', array('campaigns' => $campaignlist ) );
	}

	public function addAction( Request $request )
	{
		$campaign = new Campaign();

		$form = $this->createForm( new CampaignType(), $campaign );

		if( $request->isMethod('POST') )
		{
			$form->bind( $request );
			if( $form->isValid() )
			{
				// make sure that the IO # is unique
				$check = $this->getDoctrine()->getRepository( $this->mainRepo )->findByIonum( $campaign->getIonum() );
				if( !$check )
				{
					$timestamp = new \DateTime();
					$campaign->setAdded( $timestamp );
					$campaign->setUpdated( $timestamp );

					$em = $this->getDoctrine()->getManager();
					$em->persist( $campaign );
					$em->flush();

					$this->get('session')->getFlashBag()->add('notice', 'Your campaign has been added.');
					return $this->redirect( $this->generateUrl('dre_task_tracker_campaign') );
				}
				else
				{
                    $notice = "There is already an existing campaign with this IO #. ";
					return $this->redirect( $this->generateUrl( 'dre_task_tracker_campaign_add' ) );
				}

			}
			else
			{
				$formError = $form->getErrorsAsString();
				$notice = 'Your campaign has not been added. Please try again. ' . $formError;
				$this->get('session')->getFlashBag()->add('notice', $notice );
				return $this->render('dreTaskTrackerBundle:Campaign:add.html.twig', array(
					'form' => $form->createView()
				));
			}
		}
		else
		{
			return $this->render('dreTaskTrackerBundle:Campaign:add.html.twig', array(
				'form' => $form->createView(),
			));
		}
	} // end addAction

	public function editAction( $id )
	{
		$campaign = $this->getDoctrine()->getRepository( $this->mainRepo )->find( $id );
		if( $campaign->getDevid() == NULL )     { $campaign->setDev( 0 ); }
		if( $campaign->getReachid() == NULL )
		{
			$campaign->setReachid( 0 );
			$campaign->setReach( '' );
		}

		$form = $this->createForm( new CampaignType, $campaign );

		return $this->render('dreTaskTrackerBundle:Campaign:edit.html.twig', array(
			'form' => $form->createView(),
			'campaign' => $campaign,
		));
	} // end editAction

	public function updateAction( Request $request )
	{
		$update = new Campaign;
		$form = $this->createForm( new CampaignType, $update );

		if( $request->isMethod('POST') )
		{
			$form->bind( $request );
			if( $form->isValid() )
			{
                $campaign = $this->getDoctrine()->getRepository( $this->mainRepo )->find( $update->getId() );

				//if Dev is 1 (Dev Board), and we are updating, assign any tasks assigned to Dev Board to the new Dev
				if( $campaign->getDev()->getId() == 1 )
				{
					//lets check to see if we're updated the dev
					if( $update->getDev()->getId() != $campaign->getDev()->getId() )
					{
						foreach( $update->getTask() as $task )
						{
							if( $task->getDev()->getId() != $update->getDev()->getId() )
							{
								$task->setDevid( $update->getDev()->getId() );
							}
						}
					}
				}
				// end if Dev is 1 (Dev Board), and we are updating, assign any tasks assigned to Dev Board to the new Dev

				// update the update timestamp
				$campaign->setUpdated( new \DateTime() );
				$campaign->setIonum( $update->getIonum() );
                $campaign->setDfplineid( $update->getDfplineid() );
				$campaign->setJiraid( $update->getJiraid() );
				$campaign->setName( $update->getName() );
				$campaign->setBasecamp( $update->getBasecamp() );
				$campaign->setStart( $update->getStart() );
				$campaign->setEnd( $update->getEnd() );
				$campaign->setSov( $update->getSov() );
				$campaign->setManagerid( $update->getManager()->getId() );
				$campaign->setDevid( $update->getDev()->getId() );
				$campaign->setReachid( $update->getReach()->getId() );
                $campaign->setCoordinatorid( $update->getCoordinator()->getId() );
				//print_r( $campaign->getJiraid() );
				//$this->getDoctrine()->getRepository( $this->mainRepo )->__debug( $campaign );

				$em = $this->getDoctrine()->getManager();
				$em->flush();

				$this->get('session')->getFlashBag()->add('notice', 'Campaign information has been updated.' );
				return $this->redirect( $this->generateUrl('dre_task_tracker_campaign') );
			}
			else
			{
				$formError = $form->getErrorsAsString();
				$error_message = "Your campaign update was not saved. Please try again. " . $formError;
				$this->get('session')->getFlashBag()->add('notice', $error_message );
				return $this->render('dreTaskTracker:Campaign:edit.html.twig', array(
					'form' => $form->createView(),
				));
			} // if( $form->isValid() }
		}
		else
		{

		} // if( $request->isMethod('POST') )
	}

	private function __debug( $printme )
	{
		print_r( "<pre>" );
		print_r( $printme );
		print_r( "</pre>" );
	}
}
