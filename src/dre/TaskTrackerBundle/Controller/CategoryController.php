<?php

namespace dre\TaskTrackerBundle\Controller;

use dre\TaskTrackerBundle\Form\Type\CategoryType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use dre\TaskTrackerBundle\Entity\Category;

class CategoryController extends Controller
{
	var $categoryRepo = "dreTaskTrackerBundle:Category";

	public function indexAction($sort, Request $request)
	{
		if( !$request->query->get('dir') )	{ $dir = "ASC"; }
		else								{ $dir = $request->query->get('dir'); }
		//$categories = $this->getDoctrine()->getRepository($this->categoryRepo)->findAll();
		$categories = $this->getDoctrine()->getRepository( $this->categoryRepo )->findAllOrderBy( $sort, $dir );
		return $this->render('dreTaskTrackerBundle:Category:list.html.twig', array('categories' => $categories));
	}

	public function addAction( Request $request )
	{
		$category = new Category();

		$form = $this->CreateForm( new CategoryType(), $category );

		if( $request->isMethod('POST') )
		{   // we possibly have a new entry to add!
			$form->bind( $request );

			if( $form->isValid() )
			{   // save the new task category
				$timestamp = new \DateTime();
				$category->setAdded( $timestamp );
				$category->setUpdated( $timestamp );

				$em = $this->getDoctrine()->getManager();
				$em->persist( $category );
				$em->flush();

				$this->get('session')->getFlashBag()->add('notice', 'Your category has been added.');
				return $this->redirect( $this->generateUrl('dre_task_tracker_category') );
			}
			else
			{   // there was an error somewhere
				$formError = $form->getErrorsAsString();
				$notice = 'Your category has not been added. Please try again. ' . $formError;
				$this->get('session')->getFlashBag()->add('notice', $notice );
				return $this->render('dreTaskTrackerBundle:Category:add.html.twig', array(
					'form' => $form->createView()
				));
			}
		}
		else
		{   // we have no post values, so show the form to add a new category
			return $this->render('dreTaskTrackerBundle:Category:add.html.twig', array(
				'form' => $form->createView()
			));
		}
	}

	public function editAction( $id )
	{   // this is the view for editing
		$category = $this->getDoctrine()->getRepository( $this->categoryRepo )->find( $id );
		$form = $this->createForm( new CategoryType(), $category );

		return $this->render('dreTaskTrackerBundle:Category:edit.html.twig', array(
			'form' => $form->createView(),
			'id' => $id,
			'category' => $category,
		));
	}

	public function updateAction( Request $request )
	{   // this is the actual updating portion of edit
		$update = new Category();
		$form = $this->createForm( new CategoryType(), $update );

		if( $request->isMethod('POST') )
		{
			$form->bind( $request );
			// get the original category info so we can see what's been changed
			$category = $this->getDoctrine()->getRepository( $this->categoryRepo )->find( $update->getId() );

			if( $form->isValid() )
			{
				if( $update->getName() != $category->getName() )
				{
					$category->setName( $update->getName() );
					$category->setUpdated( new \DateTime() );
				}
				if( $update->getTip() != $category->getTip() )
				{
					$category->setTip( $update->getTip() );
					$category->setUpdated( new \DateTime() );
				}
				if( $update->getTime() != $category->getTime() )
				{
					$category->setTime( $update->getTime() );
					$category->setUpdated( new \DateTime() );
				}
				$em = $this->getDoctrine()->getManager();
				$em->flush();

				$notice = "Your category changes have been saved.";
				$this->get('session')->getFlashBag()->add('notice', $notice );
				return $this->redirect( $this->generateUrl('dre_task_tracker_category') );
			}
			else
			{
				$formError = $form->getErrorsAsString();
				$notice = 'Your category has not been updated. Please try again. ' . $formError;
				$this->get('session')->getFlashBag()->add('notice', $notice );
				return $this->redirect( $this->generateUrl('dre_task_tracker_category_edit', array(
					'id' => $update->getId()
				)) );
			}
		}
		else
		{
			return $this->redirect( $this->generateUrl('dre_task_tracker_category_edit', array(
				'id' => $update->getId()
			)) );
		}
	}

	public function deleteAction( $id )
	{   // @TODO: When I have relationships, set this to not delete if there is other items associated with this category
		$category = $this->getDoctrine()->getRepository( $this->categoryRepo )->findOneById( $id );

		$em = $this->getDoctrine()->getManager();
		$em->remove( $category );
		$em->flush();

		$notice = 'Category '. $category->getName() . ' has been deleted.';
		$this->get('session')->getFlashBag()->add( 'notice', $notice );
		return $this->redirect( $this->generateUrl('dre_task_tracker_category') );
	}
}
