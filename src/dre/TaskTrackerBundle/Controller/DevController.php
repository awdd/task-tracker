<?php

namespace dre\TaskTrackerBundle\Controller;

use dre\TaskTrackerBundle\Form\Type\DevType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use dre\TaskTrackerBundle\Entity\Dev;

class DevController extends Controller
{
	var $devRepo = "dreTaskTrackerBundle:Dev";

	public function indexAction()
	{
		$devs = $this->getDoctrine()->getRepository($this->devRepo)->findAll();
		return $this->render('dreTaskTrackerBundle:Dev:list.html.twig', array('devs' => $devs));
	}

	public function addAction( Request $request )
	{
		$dev = new Dev();

		$form = $this->CreateForm( new DevType(), $dev );

		if( $request->isMethod('POST') )
		{   // we possibly have a new entry to add!
			$form->bind( $request );

			if( $form->isValid() )
			{   // save the new task dev
				$timestamp = new \DateTime();
				$dev->setAdded( $timestamp );
				$dev->setUpdated( $timestamp );

				$em = $this->getDoctrine()->getManager();
				$em->persist( $dev );
				$em->flush();

				$this->get('session')->getFlashBag()->add('notice', 'Your dev has been added.');
				return $this->redirect( $this->generateUrl('dre_task_tracker_dev') );
			}
			else
			{   // there was an error somewhere
				$formError = $form->getErrorsAsString();
				$notice = 'Your dev has not been added. Please try again. ' . $formError;
				$this->get('session')->getFlashBag()->add('notice', $notice );
				return $this->render('dreTaskTrackerBundle:Dev:add.html.twig', array(
					'form' => $form->createView()
				));
			}
		}
		else
		{   // we have no post values, so show the form to add a new dev
			return $this->render('dreTaskTrackerBundle:Dev:add.html.twig', array(
				'form' => $form->createView()
			));
		}
	}

	public function editAction( $id )
	{   // this is the view for editing
		$dev = $this->getDoctrine()->getRepository( $this->devRepo )->find( $id );
		$form = $this->createForm( new DevType(), $dev );

		return $this->render('dreTaskTrackerBundle:Dev:edit.html.twig', array(
			'form' => $form->createView(),
			'id' => $id,
			'tasks' => $dev->getTask(),
			'campaigns' => $dev->getCampaign(),
			'dev' => $dev,
		));
	}

	public function updateAction( Request $request )
	{   // this is the actual updating portion of edit
		$update = new Dev();
		$form = $this->createForm( new DevType(), $update );

		if( $request->isMethod('POST') )
		{
			$form->bind( $request );
			// get the original dev info so we can see what's been changed
			$dev = $this->getDoctrine()->getRepository( $this->devRepo )->find( $update->getId() );

			if( $form->isValid() )
			{
				if( $update->getName() != $dev->getName() )
				{
					$dev->setName( $update->getName() );
					$dev->setUpdated( new \DateTime() );
				}
				if( $update->getBasecamp() != $dev->getBasecamp() )
				{
					$dev->setBasecamp( $update->getBasecamp() );
					$dev->setUpdated( new \DateTime() );
				}
				$em = $this->getDoctrine()->getManager();
				$em->flush();

				$notice = "Your dev changes have been saved.";
				$this->get('session')->getFlashBag()->add('notice', $notice );
				return $this->redirect( $this->generateUrl('dre_task_tracker_dev') );
			}
			else
			{
				$formError = $form->getErrorsAsString();
				$notice = 'Your dev has not been updated. Please try again. ' . $formError;
				$this->get('session')->getFlashBag()->add('notice', $notice );
				return $this->redirect( $this->generateUrl('dre_task_tracker_dev_edit', array(
					'id' => $update->getId()
				)) );
			}
		}
		else
		{
			return $this->redirect( $this->generateUrl('dre_task_tracker_dev_edit', array(
				'id' => $update->getId()
			)) );
		}
	}

	public function deleteAction( $id )
	{   // @TODO: When I have relationships, set this to not delete if there is other items associated with this dev
		$dev = $this->getDoctrine()->getRepository( $this->devRepo )->findOneById( $id );

		$em = $this->getDoctrine()->getManager();
		$em->remove( $dev );
		$em->flush();

		$notice = 'Dev '. $dev->getName() . ' has been deleted.';
		$this->get('session')->getFlashBag()->add( 'notice', $notice );
		return $this->redirect( $this->generateUrl('dre_task_tracker_dev') );
	}
}
