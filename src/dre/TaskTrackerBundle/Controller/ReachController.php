<?php
/**
 * User: donna.ryan
 * Date: 1/6/14
 * Time: 11:49 AM
 */

namespace dre\TaskTrackerBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use dre\TaskTrackerBundle\Entity\Reach;
use dre\TaskTrackerBundle\Form\Type\ReachType;


class ReachController extends Controller
{
	var $mainRepo = "dreTaskTrackerBundle:Reach";

	public function indexAction()
	{
		$reaches = $this->getDoctrine()->getRepository( $this->mainRepo )->findAll();
		return $this->render('dreTaskTrackerBundle:Reach:list.html.twig', array('reaches' => $reaches) );
	}

	public function addAction( Request $request )
	{
		$reach = new Reach();

		$form = $this->createForm( new ReachType(), $reach );

		if( $request->isMethod('POST') )
		{   // we have a new entry to add
			$form->bind( $request );
			if( $form->isValid() )
			{   // save the new entry
				$timestamp = new \DateTime();
				$reach->setAdded( $timestamp );
				$reach->setUpdated( $timestamp );

				$em = $this->getDoctrine()->getManager();
				$em->persist( $reach );
				$em->flush();

				$this->get('session')->getFlashBag()->add('notice', 'Your reach entry has been added.');
				return $this->redirect( $this->generateUrl('dre_task_tracker_reach') );
			}
			else
			{
				$formError = $form->getErrorAsString();
				$notice = 'Your reach entry has not been added. Please try again. ' . $formError;
				$this->get('session')->getFlashBag()->add('notice', $notice );
				return $this->render('dreTaskTrackerBundle:Reach:add.html.twig', array(
					'form' => $form->createView()
				));
			}
		}
		else
		{
			return $this->render('dreTaskTrackerBundle:Reach:add.html.twig', array(
				'form' => $form->createView()
			));
		}
	} // end addAction

	public function editAction( $id )
	{
		$reach = $this->getDoctrine()->getRepository( $this->mainRepo )->find( $id );
		$form = $this->createForm( new ReachType(), $reach );

		return $this->render('dreTaskTrackerBundle:Reach:edit.html.twig', array(
			'form' => $form->createView(),
			'id' => $id,
		));
	} // end editAction

	public function updateAction( Request $request )
	{
		$update = new Reach();
		$form = $this->createForm( new ReachType, $update );

		if( $request->isMethod('POST') )
		{
			$form->bind( $request );

			// get the reach info from the database so we can see what's changed
			$reach = $this->getDoctrine()->getRepository( $this->mainRepo )->find( $update->getId() );

			if( $form->isValid() )
			{   // compare the two to see if the name has changed
				if( $update->getName() != $reach->getName() )
				{
					$reach->setName( $update->getName() );
					$reach->setUpdated( new \DateTime() );
				}
				$em = $this->getDoctrine()->getManager();
				$em->flush();

				$notice = 'Your reach entry changes have been saved.';
				$this->get('session')->getFlashBag()->add('notice', $notice );
				return $this->redirect( $this->generateUrl('dre_task_tracker_reach') );
			}
			else
			{
				$formError = $form->getErrorAsString();
				$notice = 'Your reach entry changes have not been saved. Please try again. ' . $formError;
				$this->get('session')->getFlashBag()->add('notice', $notice);
				return $this->redirect( $this->generateUrl('dre_task_tracker_reach_edit', array('id' => $update->getId() )) );
			}
		}
	} // end updateAction

	// do the delete action when I can pull which tasks are associated with the selected reach entry
}
