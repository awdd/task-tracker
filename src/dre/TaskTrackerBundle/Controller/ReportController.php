<?php
/**
 * User: Donna.Ryan
 * Date: 3/28/14
 * Project: Task Tracker
 */

namespace dre\TaskTrackerBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ReportController extends Controller
{
	var $categoryRepo   = 'dreTaskTrackerBundle:Category';
	var $campaignRepo   = 'dreTaskTrackerBundle:Campaign';
	var $devRepo        = 'dreTaskTrackerBundle:Dev';
	var $managerRepo    = 'dreTaskTrackerBundle:Manager';
	var $taskRepo       = 'dreTaskTrackerBundle:Task';

	public function indexAction()
	{
		$taskYears = $this->getDoctrine()->getRepository($this->taskRepo)->getTaskYears(  );

		return $this->render('dreTaskTrackerBundle:Report:index.html.twig', array(
			'years' => $taskYears,
		));
	}

	public function byMonthAction($year, $mon)
	{
		$tasks = $this->getDoctrine()->getRepository( $this->taskRepo )->getTasksCompletedInMonth( $year, $mon );

		// now we need to do some stat-collecting on this collection of data
		$byDev = $this->getDoctrine()->getRepository( $this->devRepo )->breakoutByDev( $tasks );
		//print_r( $byDev );

		// same day turn around breakouts
		$numbers = $this->getDoctrine()->getRepository( $this->taskRepo )->breakoutByTask( $tasks );
		//print_r( $numbers );

		// breakouts by category
		$byCat = $this->getDoctrine()->getRepository( $this->categoryRepo )->breakoutByCategory( $tasks );
		//print_r( $byCat );

		return $this->render('dreTaskTrackerBundle:Report:bymonth.html.twig', array(
			'mon'   => $mon,
			'year'  => $year,
			'byDev' => $byDev,
			'tasks' => $tasks,
			'tn'    => $numbers,
			'byCat' => $byCat,
		));
	}
}
