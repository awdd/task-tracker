<?php

namespace dre\TaskTrackerBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use dre\TaskTrackerBundle\Entity\Coordinator;
use dre\TaskTrackerBundle\Form\Type\CoordinatorType;

class CoordinatorController extends Controller
{
	var $coordinatorRepo = "dreTaskTrackerBundle:Coordinator";

	public function indexAction()
	{
		$pcs = $this->getDoctrine()->getRepository($this->coordinatorRepo)->findAll();
/*
        print_r( "<pre>" );
        print_r( $pcs );
        print_r( "</pre>" );
*/
		return $this->render('dreTaskTrackerBundle:Coordinator:list.html.twig', array('coordinators' => $pcs));
	}

	public function addAction( Request $request )
	{
		$coordinator = new Coordinator();

		$form = $this->CreateForm( new CoordinatorType(), $coordinator );

		if( $request->isMethod('POST') )
		{   // we possibly have a new entry to add!
			$form->bind( $request );

			if( $form->isValid() )
			{   // save the new task dev
				$timestamp = new \DateTime();
                $coordinator->setAdded( $timestamp );
                $coordinator->setUpdated( $timestamp );

				$em = $this->getDoctrine()->getManager();
				$em->persist( $coordinator );
				$em->flush();

				$this->get('session')->getFlashBag()->add('notice', 'Your Coordinator has been added.');
				return $this->redirect( $this->generateUrl('dre_task_tracker_coordinator') );
			}
			else
			{   // there was an error somewhere
				$formError = $form->getErrorsAsString();
				$notice = 'Your Coordinator has not been added. Please try again. ' . $formError;
				$this->get('session')->getFlashBag()->add('notice', $notice );
				return $this->render('dreTaskTrackerBundle:Coordinator:add.html.twig', array(
					'form' => $form->createView()
				));
			}
		}
		else
		{   // we have no post values, so show the form to add a new dev
			return $this->render('dreTaskTrackerBundle:Coordinator:add.html.twig', array(
				'form' => $form->createView()
			));
		}
	}

	public function editAction( $id )
	{   // this is the view for editing
		$pc = $this->getDoctrine()->getRepository( $this->coordinatorRepo )->find( $id );
		$form = $this->createForm( new CoordinatorType(), $pc );

		return $this->render('dreTaskTrackerBundle:Coordinator:edit.html.twig', array(
			'form' => $form->createView(),
			'id' => $id,
			'coordinator' => $pc,
		));
	}

	public function updateAction( Request $request )
	{   // this is the actual updating portion of edit
		$update = new Coordinator();
		$form = $this->createForm( new CoordinatorType(), $update );

		if( $request->isMethod('POST') )
		{
			$form->bind( $request );
			// get the original dev info so we can see what's been changed
			$pc = $this->getDoctrine()->getRepository( $this->coordinatorRepo )->find( $update->getId() );

			if( $form->isValid() )
			{
				if( $update->getName() != $pc->getName() )
				{
					$pc->setName( $update->getName() );
					$pc->setUpdated( new \DateTime() );
				}
				$em = $this->getDoctrine()->getManager();
				$em->flush();

				$notice = "Your Coordinator changes have been saved.";
				$this->get('session')->getFlashBag()->add('notice', $notice );
				return $this->redirect( $this->generateUrl('dre_task_tracker_coordinator') );
			}
			else
			{
				$formError = $form->getErrorsAsString();
				$notice = 'Your Coordinator has not been updated. Please try again. ' . $formError;
				$this->get('session')->getFlashBag()->add('notice', $notice );
				return $this->redirect( $this->generateUrl('dre_task_tracker_coordinator_edit', array(
					'id' => $update->getId()
				)) );
			}
		}
		else
		{
			return $this->redirect( $this->generateUrl('dre_task_tracker_coordinator_edit', array(
				'id' => $update->getId()
			)) );
		}
	}

	public function deleteAction( $id )
	{   // @TODO: When I have relationships, set this to not delete if there is other items associated with this Coordinator
		$pc = $this->getDoctrine()->getRepository( $this->coordinatorRepo )->findOneById( $id );

		$em = $this->getDoctrine()->getManager();
		$em->remove( $pc );
		$em->flush();

		$notice = 'Coordinator '. $pc->getName() . ' has been deleted.';
		$this->get('session')->getFlashBag()->add( 'notice', $notice );
		return $this->redirect( $this->generateUrl('dre_task_tracker_coordinator') );
	}
}
