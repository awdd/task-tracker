<?php

namespace dre\TaskTrackerBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
	var $categoryRepo   = 'dreTaskTrackerBundle:Category';
	var $campaignRepo   = 'dreTaskTrackerBundle:Campaign';
	var $devRepo        = 'dreTaskTrackerBundle:Dev';
	var $managerRepo    = 'dreTaskTrackerBundle:Manager';
	var $taskRepo       = 'dreTaskTrackerBundle:Task';

    public function indexAction()
    {
	    $today      = new \DateTime();
	    $week       = new \DateTime();
	    $week->add( new \DateInterval("P7D") );

	    // Devs
	    $devs = $this->getDoctrine()->getRepository($this->devRepo)->findAll();

	    // --- CAMPAIGNS ----------------------------------------------------------------------------------------
	    $campaign_count = $this->getDoctrine()->getRepository( $this->campaignRepo )->countActive( '' );
	    foreach( $devs as $k=>$v )
	    {
		    $devCampaigns[$k]['id'] = $v->getId();
		    $devCampaigns[$k]['name'] = $v->getName();
		    $devCampaigns[$k]['count'] = $this->getDoctrine()->getRepository( $this->campaignRepo )->countActive( $v->getId() );
	    }
	    $campaigns2 = $this->getDoctrine()->getRepository($this->campaignRepo)->findAllByDateRange('end', $today->format('Y-m-d 00:00:00'), $today->format('Y-m-d 00:00:00') );
	    if( $campaigns2 != '' )
	    { $ending = $this->getDoctrine()->getRepository($this->campaignRepo)->listifyItems( $campaigns2 ); }
	    $campaigns1 = $this->getDoctrine()->getRepository($this->campaignRepo)->findAllOrderBy('id', 'DESC', 'active');
	    if( $campaigns1 != '' )
	    { $active = $this->getDoctrine()->getRepository($this->campaignRepo)->listifyItems( $campaigns1 ); }

	    // --- TASKS --------------------------------------------------------------------------------------------
	    $task_count = $this->getDoctrine()->getRepository( $this->taskRepo )->countActive( '' );
	    foreach( $devs as $k=>$v )
	    {
		    //print_r( $v );
		    $devTasks[$k]['id'] = $v->getId();
		    $devTasks[$k]['name'] = $v->getName();
		    $devTasks[$k]['count'] = $this->getDoctrine()->getRepository( $this->taskRepo )->countActive( $v->getId() );
	    }
	    //$tasks     = $this->getDoctrine()->getRepository($this->taskRepo)->findIncomplete( 'due', 'ASC' );
	    $overdue    = $this->getDoctrine()->getRepository($this->taskRepo)->findIncompleteOverdue('due', 'ASC', $today->format('Y-m-d 00:00:00') );
	    $upcoming   = $this->getDoctrine()->getRepository($this->taskRepo)->findIncompleteForDateRange( 'due', 'ASC', $today->format('Y-m-d 00:00:00'), $week->format('Y-m-d 00:00:00'));

		// Rendering
        return $this->render('dreTaskTrackerBundle:Default:index.html.twig', array(
	        'total_campaigns'     => $campaign_count,
	        'total_tasks'         => $task_count,
	        'count_tasks'         => $devTasks,
	        'count_campaigns'     => $devCampaigns,
	        'overdue'             => $overdue,
	        'upcoming'            => $upcoming, //$upcoming,
	        'cactive'             => $active,
	        'cending'             => $ending,
	        'display'             => 'incomplete',
        ));
    }

	public function searchAction( Request $request  )
	{
		//$this->__debug( $request );
		if( $request->isMethod('POST') )
		{
			$searchfor = $request->request->get('searchfor');
		}
		elseif( $request->isMethod('GET') )
		{
			$searchfor = $request->query->get('searchfor');
		}
		$categories = $this->getDoctrine()->getRepository($this->categoryRepo)->findAllBySearchTerm( $searchfor );
		$campaigns1 = $this->getDoctrine()->getRepository($this->campaignRepo)->findAllBySearchTerm( $searchfor );
		$devs       = $this->getDoctrine()->getRepository($this->devRepo)->findAllBySearchTerm( $searchfor );
		$managers   = $this->getDoctrine()->getRepository($this->managerRepo)->findAllBySearchTerm( $searchfor );
		$tasks1     = $this->getDoctrine()->getRepository($this->taskRepo)->findAllBySearchTerm( $searchfor );
		if( $campaigns1 != '' )
		{
			$campaigns = $this->getDoctrine()->getRepository($this->campaignRepo)->listifyItems( $campaigns1 );
		}
		if( $tasks1 != '' )
		{
			$tasks      = $this->getDoctrine()->getRepository($this->taskRepo)->listifyTasks( $tasks1 );
		}

		return $this->render('dreTaskTrackerBundle:Default:search-results.html.twig', array(
			'searchfor'     => $searchfor,
			'categories'    => $categories,
			'campaigns'     => $campaigns,
			'devs'          => $devs,
			'managers'      => $managers,
			'tasks'         => $tasks,
			'display'       => '',
		));
	}

	private function __debug( $print )
	{
		print_r( "<pre>" );
		print_r( $print );
		print_r( "</pre>" );
	}

	public function setFlashMessage( $message )
	{
		$this->get('session')->getFlashBag()->add('notice', $message );
	}
}
