<?php
/**
 * User: donna.ryan
 * Date: 1/9/14
 * Project: Task Tracker
 */

namespace dre\TaskTrackerBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use dre\TaskTrackerBundle\Entity\Task;
use dre\TaskTrackerBundle\Entity\Campaign;
use dre\TaskTrackerBundle\Form\Type\TaskType;

class TaskController extends Controller
{
	var $mainRepo       = "dreTaskTrackerBundle:Task";
	var $campaignRepo   = "dreTaskTrackerBundle:Campaign";

	public function indexAction( $display, $sort, Request $request )
	{
		if( !$request->query->get('dir') )	{ $dir = "ASC"; }
		else								{ $dir = $request->query->get('dir'); }
		if( $display == "incomplete" )
		{
			$tasklist = $this->getDoctrine()->getRepository( $this->mainRepo )->findIncomplete( $sort, $dir );
		}
		elseif( $display == "complete" )
		{
			$tasklist = $this->getDoctrine()->getRepository( $this->mainRepo )->findComplete( $sort, $dir );
		}


		//$this->__debug( $tasklist );

		return $this->render('dreTaskTrackerBundle:Task:list.html.twig', array(
			'tasks' => $tasklist,
			'tasktype' => ' ',
			'display' => $display,
		));
	} // end indexAction

	public function addAction( Request $request, $template, $id )
	{
		$task = new Task();
		if( ($template == 'modal') && (!$request->isMethod('POST')) )
		{
			// preset the campaign, manager and dev
			$campaign = $this->getDoctrine()->getRepository( $this->campaignRepo )->find( $id );
			$task->setCampaign( $campaign );
			$task->setDev( $campaign->getDev() );
			$task->setManager( $campaign->getManager() );
		}

		$form = $this->createForm( new TaskType, $task );

		if( $request->isMethod('POST') )
		{
			$form->bind( $request );

			if( $form->isValid() )
			{
				$campaignid = $task->getCampaign()->getId();
				$task->setCampaignid( $task->getCampaign()->getId() );
				$task->setUpdated( new \DateTime() );

				$em = $this->getDoctrine()->getManager();
				$em->persist( $task );
				$em->flush();

				$this->get('session')->getFlashBag()->add('notice', "Your task was added.");
				if( $template == 'modal' )
				{
					return $this->redirect( $this->generateUrl( 'dre_task_tracker_campaign_edit', array( 'id' => $task->getCampaign()->getId() ) ) );
				}
				else
				{
					return $this->redirect( $this->generateUrl('dre_task_tracker_task') );
				}

			}
			else
			{
				$formError = $form->getErrorsAsString();
				$notice = 'Your task has not been added. Please try again. ' . $formError;
				$this->get('session')->getFlashBag()->add('notice', $notice );
				if( $template == 'modal' )
				{
					return $this->render('dreTaskTrackerBundle:Task:add.html.twig', array(
						'form' => $form->createView(),
						'noticeclass' => 'error',
					));
				}
				else
				{
					return $this->render('dreTaskTrackerBundle:Task:add.html.twig', array(
						'form' => $form->createView(),
						'noticeclass' => 'error',
					));
				}
			}
		}
		else
		{
			if( $template == 'modal' )
			{
				return $this->render('dreTaskTrackerBundle:Task:add-modal.html.twig', array(
					'form' => $form->createView()
				));
			}
			else
			{
				return $this->render('dreTaskTrackerBundle:Task:add.html.twig', array(
					'form' => $form->createView()
				));
			}
		} // end if( $request->isMethod('POST') )
	} // end addAction

    public function editAction( $id, $template, $redir )
    {
        // define our templates here
        switch($template)
        {
            case "modal":
                $editTemplate = "dreTaskTrackerBundle:Task:edit-modal.html.twig";
                break;
            case "list":
                $editTemplate = "dreTaskTrackerBundle:Task:edit-list.html.twig";
                break;
            case "page";
            default:
                $editTemplate = "dreTaskTrackerBundle:Task:edit.html.twig";
        }
        $task = $this->getDoctrine()->getRepository( $this->mainRepo )->find( $id );

        $form = $this->createForm( new TaskType, $task );
        //$this->__debug( $task );
        return $this->render($editTemplate, array(
            'form' => $form->createView(),
            'task' => $task,
            'today' => date("m-d-Y", mktime(date("h"-3), 0, 0, date("m"), date("d"), date("Y"))),
	        'redir' => $redir,
        ));
    }   // end editAction

    public function updateAction( Request $request )
    {
        $update = new Task();
        $form = $this->createForm( new TaskType, $update );

	    //$this->__debug( $request->request->get('redir') );

        if( $request->isMethod('POST') )
        {
            $form->bind( $request );
            if( $form->isValid() )
            {
                $task = $this->getDoctrine()->getRepository($this->mainRepo)->find($update->getId());

	            if( $task->getDue() != $update->getDue() )
	            {
		            $task->setDueupdates( $task->getDueupdates() + 1 );
	            }
                $task->setDue( $update->getDue() );
	            $task->setRunstart( $update->getRunstart() );
	            $task->setRunend( $update->getRunend() );
                $task->setAdded( $update->getAdded() );
                $task->setAssets( $update->getAssets() );
                $task->setCompleted( $update->getCompleted() );

	            $task->setName( $update->getName() );
	            $task->setBasecamp( $update->getBasecamp() );

                $task->setManagerid( $update->getManager()->getId() );
                $task->setDevid( $update->getDev()->getId() );
                $task->setCategoryid( $update->getCategory()->getId() );
	            $task->setCampaignid( $update->getCampaign()->getId() );

				// update the update timestamp
                $task->setUpdated();

                $em = $this->getDoctrine()->getManager();
                $em->flush();

	            $this->get('session')->getFlashBag()->add('notice', 'Task information has been updated.' );
	            if( $request->request->get('redir') == 'campaign')
	            {
		            return $this->redirect( $this->generateUrl( 'dre_task_tracker_campaign_edit', array( 'id' => $task->getCampaign()->getId() ) ) );
	            }
	            else
	            {
		            return $this->redirect( $this->generateUrl('dre_task_tracker_task') );
	            }
            }
            else
            {
                $formError = $form->getErrorsAsString();
                $error_message = "Your task update was not saved. Please try again. " . $formError;
                $this->get('session')->getFlashBag()->add('notice', $error_message );
                return $this->render('dreTaskTracker:Task:edit.html.twig', array(
                    'form' => $form->createView(),
                ));
            } // if( $form->isValid() }
        }
        else
        {

        } // if( $request->isMethod('POST') )
    } // end updateAction


	public function deleteAction( $id, $redir )
	{
		$task = $this->getDoctrine()->getRepository($this->mainRepo)->findOneById( $id );

		$em = $this->getDoctrine()->getManager();
		$em->remove( $task );
		$em->flush();

		$this->setFlashMessage( 'Task ' . $task->getName() . ' has been deleted.');
		if( $redir == "campaign" )
		{
			return $this->redirect( $this->generateUrl( 'dre_task_tracker_campaign_edit', array( 'id' => $task->getCampaign()->getId() ) ) );
		}
		else
		{
			return $this->redirect( $this->generateUrl('dre_task_tracker_task') );
		}

	} // end deleteAction


	private function __debug( $print )
	{
		print_r( "<pre>" );
		print_r( $print );
		print_r( "</pre>" );
	}

	public function setFlashMessage( $message )
	{
		$this->get('session')->getFlashBag()->add('notice', $message );
	}

}
