<?php
/**
 * User: donna.ryan
 * Date: 1/6/14
 * Time: 11:55 AM
 */

namespace dre\TaskTrackerBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ReachType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
		->add('name', 'text')
		->add('id', 'hidden', array('required' => false))
		;
	}

	public function getName()
	{
		return "reeach";
	}
}