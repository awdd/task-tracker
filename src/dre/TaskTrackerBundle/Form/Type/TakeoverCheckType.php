<?php
/**
 * User: donna.ryan
 * Date: 3/2/2015
 * Project: Task Tracker
 */

namespace dre\TaskTrackerBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class TakeoverCheckType extends AbstractType
{
	public function buildForm( FormBuilderInterface $builder, array $options )
	{
		$builder
			->add('copyliveurl', 'checkbox')
			->add('sponsorship', 'checkbox')
			->add('starttime', 'checkbox')
			->add('endtime', 'checkbox')
			->add('targetinventory', 'checkbox')
			->add('targetgeo', 'checkbox')
			->add('targetdevice', 'checkbox')
			->add('creatives', 'checkbox')
			->add('multiple300x250', 'checkbox')

			->add('copyqaurl', 'checkbox')
			->add('qatest', 'checkbox')
			->add('qastarttime', 'checkbox')
			->add('qasponsorship', 'checkbox')
			->add('qaendtime', 'checkbox')
			->add('qatargetrate', 'checkbox')
			->add('qatargetcn', 'checkbox')
			->add('qatargetgeo', 'checkbox')
			->add('qaarchive', 'checkbox')
		;
	}

	public function getName()
	{
		return "takeovercheck";
	}

}
