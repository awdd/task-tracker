<?php
namespace dre\TaskTrackerBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CategoryType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options )
	{
		$builder
		->add('name', 'text')
		->add('tip', 'text', array('required'=>false))
		->add('time', 'text', array('required'=>false))
		->add('id', 'hidden', array('required'=>false))
		;
	}

	public function getName()
	{
		return 'category';
	}
}
