<?php
/**
 * User: donna.ryan
 * Date: 1/6/14
 * Time: 5:17 PM
 */

namespace dre\TaskTrackerBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;


class TaskType extends AbstractType
{
	public function buildForm( FormBuilderInterface $builder, array $options )
	{
		// set current date
		$currentmonth = date('M');
		$currentyear = date('Y');
		$currentday = date('j');

		$builder
		->add('name', 'text')
		->add('due', 'date', array(
			'input'  => 'datetime',
			'widget' => 'single_text',
		))
		->add('added', 'date', array(
			'input'  => 'datetime',
			'widget' => 'single_text',
			//'data' => new \DateTime(),
		))
		->add('assets', 'date', array(
			'input'  => 'datetime',
			'widget' => 'single_text',
			'required' => false
		))
		->add('completed', 'date', array(
			'input'  => 'datetime',
			'widget' => 'single_text',
			'required' => false,
		))
		->add('runstart', 'date', array(
			'input'  => 'datetime',
            'label' => 'Start',
			'widget' => 'single_text',
			'required' => false,
		))
		->add('runend', 'date', array(
			'input'  => 'datetime',
            'label' => 'End',
			'widget' => 'single_text',
			'required' => false,
		))
		->add('campaign', 'entity', array(
				 'required' => 'false',
				 'class' => 'dreTaskTrackerBundle:Campaign',
				 'property' => 'name',
				 'empty_value' => 'Choose a campaign',

			))
        ->add('manager', 'entity', array(
            'required' => 'false',
            'class' => 'dreTaskTrackerBundle:Manager',
            'property' => 'name',
            'empty_value' => 'Choose a manager',

        ))
        ->add('dev', 'entity', array(
            'class' => 'dreTaskTrackerBundle:Dev',
            'property' => 'name',
            'empty_value' => 'Choose a developer',

        ))
		->add('category', 'entity', array(
			'class' => 'dreTaskTrackerBundle:Category',
			'property' => 'name',
			'empty_value' => 'Choose a category',

		))
		->add('basecamp', 'text', array('required' => false) )
		->add('id', 'hidden', array('required' => false) )
		;
	}

	public function getDefaultOptions(array $options)
	{
		return array(
			'data_class' => 'dre\TaskTrackerBundle\Entity\Campaign',
			'data_class' => 'dre\TaskTrackerBundle\Entity\Manager',
			'data_class' => 'dre\TaskTrackerBundle\Entity\Dev',
			'data_class' => 'dre\TaskTrackerBundle\Entity\Category',
		);
	}

	public function getName()
	{
		return 'task';
	}
}
