<?php
/**
 * User: donna.ryan
 * Date: 1/6/14
 * Time: 5:17 PM
 */

namespace dre\TaskTrackerBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;


class CampaignType extends AbstractType
{
	public function buildForm( FormBuilderInterface $builder, array $options )
	{
		$builder
		->add('name', 'text')
		->add('ionum', 'text', array('required' => false) )
        ->add('dfplineid', 'text', array('required' => false) )
		->add('jiraid', 'text', array('required' => false) )
		->add('basecamp', 'text', array('required' => false) )
		->add('start', 'date', array(
			'input'  => 'datetime',
			'widget' => 'single_text',
		))
		->add('end', 'date', array(
			'input'  => 'datetime',
			'widget' => 'single_text',
		))
		->add('reach', 'entity', array(
			'required' => 'false',
			'class' => 'dreTaskTrackerBundle:Reach',
			'property' => 'name',
			'empty_value' => 'Select a reach category',
		))
		->add('sov', 'text', array('required' => false) )
		->add('manager', 'entity', array(
			'required' => 'false',
			'class' => 'dreTaskTrackerBundle:Manager',
			'property' => 'name',
			'empty_value' => 'Select a manager',
		))
        ->add('dev', 'entity', array(
            'required' => 'false',
            'class' => 'dreTaskTrackerBundle:Dev',
            'property' => 'name',
            'empty_value' => 'Select a developer',
        ))
        ->add('coordinator', 'entity', array(
            'required' => 'false',
            'class' => 'dreTaskTrackerBundle:Coordinator',
            'property' => 'name',
            'empty_value' => 'Select a manager',
        ))
		->add('id', 'hidden', array('required' => false) )
		;
	}

	public function getDefaultOptions(array $options)
	{
		return array(
			'data_class' => 'dre\TaskTrackerBundle\Entity\Reach',
			'data_class' => 'dre\TaskTrackerBundle\Entity\Manager',
			'data_class' => 'dre\TaskTrackerBundle\Entity\Dev',
            //'data_class' => 'dre\TaskTrackerBundle\Entity\Coordinator',
		);
	}

	public function getName()
	{
		return 'campaign';
	}
}
