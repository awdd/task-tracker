<?php
namespace dre\TaskTrackerBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class DevType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options )
	{
		$builder
		->add('name', 'text')
		->add('basecamp', 'text', array('required'=>false))
		->add('id', 'hidden', array('required'=>false))
		;
	}

	public function getName()
	{
		return 'dev';
	}
}