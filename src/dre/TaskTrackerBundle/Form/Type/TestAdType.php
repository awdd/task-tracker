<?php
/**
 * User: donna.ryan
 * Date: 10/21/2014
 * Project: Task Tracker
 */

namespace dre\TaskTrackerBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;


class TestAdType extends AbstractType
{
	public function buildForm( FormBuilderInterface $builder, array $options )
	{
		$builder
			->add('testad', 'textarea')
			->add('showad', 'checkbox', array('required' => false) )
		;
	}

	public function getName()
	{
		return "testad";
	}
}
